import { User } from '@prisma/client'
import { Request, Response } from 'express'
import { validationResult } from 'express-validator'
import { IUsersResponse } from '../interfaces/users'
import { prisma } from '../libs'

export class UsersController {
  async getMe(req: Request, res: Response<IUsersResponse>) {
    try {
      const id = req.userId
      const user = await prisma.user.findUnique({
        where: { id },
      })
      if (!user) {
        return res
          .status(404)
          .json({ success: false, message: 'User not found.' })
      }
      return res.status(200).json({ success: true, nickname: user.nickname })
    } catch (error) {
      return res
        .status(500)
        .json({ success: false, message: 'Something went wrong :(' })
    }
  }

  async updateNickname(
    req: Request<{}, {}, User>,
    res: Response<IUsersResponse>
  ) {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      return res.status(400).json({ success: false, errors: errors.array() })
    }
    try {
      const id = req.userId
      const { nickname } = req.body
      await prisma.user.update({
        where: { id },
        data: { nickname },
      })
      return res.status(200).json({ success: true, nickname })
    } catch (error) {
      return res
        .status(500)
        .json({ success: false, message: 'Something went wrong :(' })
    }
  }
}
