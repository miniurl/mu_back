import { ValidationError } from 'express-validator'

export interface IResponse {
  success: boolean
  message?: string
  errors?: ValidationError[]
}
