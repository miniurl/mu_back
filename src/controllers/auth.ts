import { User } from '@prisma/client'
import { Request, Response } from 'express'
import { validationResult } from 'express-validator'
import { IAuthResponse } from 'src/interfaces'
import { prisma, generateToken } from '../libs'

export class AuthController {
  async auth(req: Request<{}, {}, User>, res: Response<IAuthResponse>) {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      return res.status(400).json({ success: false, errors: errors.array() })
    }

    try {
      const { name, email, provider } = req.body
      const nickname = name.split(' ')[0].toLowerCase()

      const user = await prisma.user.upsert({
        where: {
          email,
        },
        update: {
          name,
        },
        create: {
          email,
          name,
          provider,
          nickname,
        },
      })

      const token = generateToken(user.id)

      return res.json({ success: true, token })
    } catch (error) {
      return res
        .status(500)
        .json({ success: false, message: 'Something went wrong :(' })
    }
  }
}
