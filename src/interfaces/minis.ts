import { Mini } from '@prisma/client'
import { IResponse } from './generics'

export interface IMinisResponse extends IResponse {
  mini?: Mini & { mini: string }
  minis?: Mini[]
}

export interface IMiniResponse extends IResponse {
  url?: string
}
