import { Router } from 'express'
import { body } from 'express-validator'
import { UsersController } from '../controllers/users'
import { AuthMiddleware } from '../middleware'

export const UsersRouter = () => {
  const router = Router()
  const usersController = new UsersController()

  router.get('/me', AuthMiddleware, usersController.getMe)
  router.put(
    '/',
    AuthMiddleware,
    body('nickname').notEmpty().withMessage('You must provide a nickname'),
    usersController.updateNickname
  )

  return router
}
