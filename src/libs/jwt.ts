import jwt from 'jsonwebtoken'
import { env } from '../config'

export const generateToken = (sub: string) => {
  return jwt.sign({ sub }, env.JWT_SECRET, {
    algorithm: 'HS256',
    expiresIn: '7d',
  })
}

export const decodeToken = (token: string): string => {
  try {
    const decodedToken = jwt.verify(token, env.JWT_SECRET) as jwt.JwtPayload
    return decodedToken.sub || ''
  } catch (error) {
    return ''
  }
}
