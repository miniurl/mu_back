import { Router } from 'express'
import { AuthController } from '../controllers'
import { body } from 'express-validator'

export const AuthRouter = () => {
  const router = Router()
  const authController = new AuthController()
  router.post(
    '/',
    body('name').notEmpty().withMessage('You must provide a name'),
    body('email').isEmail().withMessage('You must provide a valid email'),
    body('provider').notEmpty().withMessage('You must provide a provider'),
    authController.auth
  )

  return router
}
