import { IResponse } from './generics'

export interface IAuthResponse extends IResponse {
  token?: string
}
