import { NextFunction, Request, Response } from 'express'
import { IResponse } from '../interfaces/generics'
import { decodeToken } from '../libs'

export const AuthMiddleware = (
  req: Request,
  res: Response<IResponse>,
  next: NextFunction
) => {
  try {
    const token = req.headers.authorization?.split(' ')[1] || '' // Bearer [token]
    const userId = decodeToken(token)
    if (!userId) {
      return res.status(401).json({
        success: false,
        message: 'Log in to continue!',
      })
    }
    req.userId = userId
    return next()
  } catch (error) {
    return res.status(401).json({
      success: false,
      message: 'Log in to continue!',
    })
  }
}
