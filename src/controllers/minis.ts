import { Mini } from '@prisma/client'
import { Request, Response } from 'express'
import { validationResult } from 'express-validator'
import { base62 } from '../helpers'
import { IMiniResponse, IMinisResponse } from '../interfaces/minis'
import { prisma } from '../libs'

export class MinisController {
  async getMinis(req: Request, res: Response<IMinisResponse>) {
    try {
      const id = req.userId
      const minis = (await prisma.mini.findMany({
        where: { userId: id },
        select: {
          id: true,
          url: true,
          createdAt: true,
        },
      })) as Mini[]

      return res.status(200).json({
        success: true,
        minis: minis.map((mini) => {
          return { ...mini, mini: base62.encode(mini.id) }
        }),
      })
    } catch (error) {
      return res
        .status(500)
        .json({ success: false, message: 'Something went wrong :(' })
    }
  }

  async createMini(req: Request<{}, {}, Mini>, res: Response<IMinisResponse>) {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      return res.status(400).json({ success: false, errors: errors.array() })
    }

    try {
      const { url } = req.body
      const userId = req.userId!

      const minisCount = await prisma.mini.aggregate({
        where: { userId },
        _count: { id: true },
      })

      if (minisCount._count.id >= 10) {
        return res.status(400).json({
          success: false,
          message: 'You already have 10 minis!',
        })
      }

      const mini = (await prisma.mini.create({
        data: { url, userId },
        select: {
          id: true,
          url: true,
          createdAt: true,
        },
      })) as Mini
      return res.status(201).json({
        success: true,
        mini: { ...mini, mini: base62.encode(mini.id) },
      })
    } catch (error) {
      return res
        .status(500)
        .json({ success: false, message: 'Something went wrong :(' })
    }
  }

  async deleteMini(req: Request, res: Response<IMinisResponse>) {
    try {
      const userId = req.userId
      const { id } = req.params
      const mini = await prisma.mini.deleteMany({
        where: { id: parseInt(id), userId },
      })
      if (!mini.count) {
        return res
          .status(404)
          .json({ success: false, message: 'Mini not found :(' })
      }
      return res.status(200).json({ success: true })
    } catch (error) {
      return res
        .status(500)
        .json({ success: false, message: 'Something went wrong :(' })
    }
  }

  async getMini(req: Request, res: Response<IMiniResponse>) {
    try {
      const { mini } = req.params
      const id = base62.decode(mini)
      const result = await prisma.mini.findUnique({
        where: { id },
        select: {
          url: true,
        },
      })
      if (!result) {
        return res
          .status(404)
          .json({ success: false, message: 'Mini not found :(' })
      }
      return res.status(200).json({ success: true, url: result.url })
    } catch (error) {
      return res
        .status(500)
        .json({ success: false, message: 'Something went wrong :(' })
    }
  }
}
