import 'dotenv/config'
import express from 'express'
import cors from 'cors'
import morgan from 'morgan'
import { env } from './config'
import { AuthRouter, MinisRouter, UsersRouter } from './routes'

const main = () => {
  const app = express()

  // Middleware
  app.use(cors())
  app.use(express.json())
  app.use(morgan('dev'))

  // Routes
  app.use('/auth', AuthRouter())
  app.use('/users', UsersRouter())
  app.use('/minis', MinisRouter())

  app.listen(env.PORT, () => {
    console.log(`Server is up on  http://localhost:${env.PORT}`)
  })
}

main()
