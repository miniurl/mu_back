import { User } from '@prisma/client'
import { IResponse } from './generics'

export interface IUsersResponse extends IResponse {
  nickname?: string
}
