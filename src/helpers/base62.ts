export const base62 = {
  charset:
    '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'.split(''),
  encode: (id: number) => {
    if (id === 0) {
      return '0'
    }
    let s: Array<string> = []
    while (id > 0) {
      s = [base62.charset[id % 62], ...s]
      id = Math.floor(id / 62)
    }
    return s.join('')
  },
  decode: (mini: string) =>
    mini
      .split('')
      .reverse()
      .reduce(
        (prev, curr, i) => prev + base62.charset.indexOf(curr) * 62 ** i,
        0
      ),
}
