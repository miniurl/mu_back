import { Router } from 'express'
import { AuthMiddleware } from '../middleware'
import { MinisController } from '../controllers'
import { body } from 'express-validator'

export const MinisRouter = () => {
  const router = Router()
  const minisController = new MinisController()

  router.get('/', AuthMiddleware, minisController.getMinis)
  router.get('/:mini', minisController.getMini)
  router.post(
    '/',
    body('url').isURL().withMessage('You must provide a valid url.'),
    AuthMiddleware,
    minisController.createMini
  )
  router.delete('/:id', AuthMiddleware, minisController.deleteMini)

  return router
}
